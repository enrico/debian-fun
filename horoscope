#!/usr/bin/python3

# TODO:
#  - Filter packages by tags (not just role::program)
#  - Go from packages to tags
#  - Build a tag cloud or a word cloud
#  - Extract significant phrases from significant package descriptions

import gzip
import math
import os.path
import apt
import sys
from debian import debtags
from optparse import OptionParser
import io
import xdg.BaseDirectory
import urllib.request
import logging

log = logging.getLogger("horoscope")


def get_by_inst():
    """
    Acquire, cache and parse the by_inst file
    """
    cachedir = xdg.BaseDirectory.save_cache_path("debian-fun")
    by_inst = os.path.join(cachedir, "by_inst.gz")
    # TODO: also redownload if cached version is too old
    if os.path.exists(by_inst):
        with open(by_inst, "rb") as fd:
            data = fd.read()
    else:
        url = "http://popcon.debian.org/by_inst.gz"
        log.info("Downloading %s...", url)
        try:
            fd = urllib.request.urlopen(url)
            data = fd.read()
        finally:
            fd.close()
        with open(by_inst, "wb") as fd:
            fd.write(data)

    zfd = io.BytesIO(data)
    with gzip.open(zfd, "rt") as fd:
        for line in fd:
            if line[0] in "#-":
                continue
            fields = line.split()
            if fields[1] == "Total":
                continue
            inst = float(fields[2])
            pkg = fields[1]
            yield pkg, inst


def get_tags():
    """
    Get the Debtags database
    """
    if not os.path.exists('/var/lib/debtags/package-tags'):
        sys.stderr.write("Sorry, /var/lib/debtags/package-tags doesn't exist and is required")
        sys.exit(2)

    db = debtags.DB()
    with open('/var/lib/debtags/package-tags', "rt") as fd:
        db.read(fd)
    return db


def parse_vote(fd):
    """
    Parse a popcon vote file, generating the names of the valid packages in
    the vote
    """
    for line in fd:
        if line.startswith("POPULARITY"):
            continue
        elif line.startswith("END-POPULARITY"):
            continue
        else:
            data = line[:-1].split(" ")
            if len(data) < 4:
                continue
            if data[3] == '<NOFILES>':
                # Empty/virtual packages
                yield data[2], 0.1
            elif len(data) == 4:
                # Used packages
                yield data[2], 1.
            elif data[4] == '<OLD>':
                # Unused packages
                yield data[2], 0.3
            elif data[4] == '<RECENT-CTIME>':
                # Recently installed packages
                yield data[2], 0.8


def get_popcon():
    if not os.path.exists('/var/log/popularity-contest'):
        sys.stderr.write("Sorry, /var/log/popularity-contest does not exist: you need to participate in the popularity contest to use this command")
        sys.exit(2)

    with open("/var/log/popularity-contest") as infd:
        for pkg, tf in parse_vote(infd):
            yield pkg, tf


class Horoscope(object):
    def __init__(self):
        pass

    def compute_horoscope(self):
        total = 0
        count = dict()
        for pkg, inst in get_by_inst():
            total = max(total, inst)
            count[pkg] = inst

        apt_cache = apt.Cache()
        tfidf = []

        db = get_tags()

        # if (opts.tagoutput):
        #    # TODO do stuff
        #    pass

        for pkg, tf in get_popcon():
            if pkg not in apt_cache:
                continue
            aptpkg = apt_cache[pkg]
            if not aptpkg.installed:
                continue
            if 'role::program' not in db.tags_of_package(pkg):
                continue
            pkgcount = count.get(pkg, None)
            if pkgcount is None:
                print("no count found for", pkg)
            else:
                tfidf.append((pkg, tf * math.log(total / pkgcount)))

        tfidf.sort(key=lambda x: x[1], reverse=True)

        for rank, word in enumerate(tfidf[:opts.count]):
            pkg = apt_cache[word[0]]
            print(rank+1, word[0], '-', pkg.candidate.summary)


if __name__ == "__main__":
    class Parser(OptionParser):
        def __init__(self, *args, **kwargs):
            OptionParser.__init__(self, *args, **kwargs)

        def error(self, msg):
            sys.stderr.write("%s: error: %s\n\n" % (self.get_prog_name(), msg))
            self.print_help(sys.stderr)
            sys.exit(2)

    parser = Parser(usage="usage: %prog [options] filename",
                    version="%prog 0.1",
                    description="Generate package or tag usage profile")
    parser.add_option("--packages", action="store_true", dest="packageoutput", default=True,
                      help="Show package profiles (default: %default)")
    parser.add_option("--tags", action="store_true", dest="tagoutput", default=False,
                      help="Show tag profiles (default: %default)")
    parser.add_option("--count", default=10, help="Number of items to show (default: %default)")

    (opts, args) = parser.parse_args()

    FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
    # if opts.debug:
    #     logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
    # elif opts.verbose:
    #     logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)
    # else:
    #     logging.basicConfig(level=logging.WARN, stream=sys.stderr, format=FORMAT)
    logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

    opts.count = int(opts.count)

    if (not opts.tagoutput and not opts.packageoutput):
        parser.error("You must choose a type of output")

    if (opts.tagoutput):
        # TODO remove me later
        sys.stderr.write("Tag output not yet implemented")
        sys.exit(2)

    horoscope = Horoscope()
    horoscope.compute_horoscope()
